package com.orbita.innovacion.proyinte;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;

public class pin_cambio extends Fragment {

    private String PINPassword = null;

    private PinEntryEditText pin, nuevo, verificar;
    private Button cambiar;
    private LinearLayout layoutPin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pin_cambio, container, false);

        PINPassword = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("PINPassword","null");


        layoutPin = (LinearLayout) v.findViewById(R.id.LayoutPinCambio);
        pin = (PinEntryEditText) v.findViewById(R.id.txt_pin_entry);
        nuevo = (PinEntryEditText) v.findViewById(R.id.txt_pin_2);
        verificar = (PinEntryEditText) v.findViewById(R.id.txt_pin_3);
        cambiar = (Button) v.findViewById(R.id.btn_cambiar);

        if(PINPassword.equals("null")){
            pin.setVisibility(View.GONE);
            nuevo.setEnabled(true);
            verificar.setEnabled(true);
            cambiar.setText("Activar");
        }else{
            if (pin != null) {
                pin.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                    @Override
                    public void onPinEntered(CharSequence str) {
                        if (str.toString().equals(PINPassword)) {
                            pin.setEnabled(false);
                            nuevo.setEnabled(true);
                            verificar.setEnabled(true);
                        } else {
                            Toast.makeText(getContext(), "Pin incorrecto", Toast.LENGTH_SHORT).show();
                            pin.setText(null);
                        }
                    }
                });
            }
        }

        cambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(nuevo.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo de vacío", Toast.LENGTH_SHORT).show();
                }else if(verificar.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo vacío", Toast.LENGTH_SHORT).show();
                }else {
                    if(nuevo.getText().toString().equals(verificar.getText().toString())){
                        sharedPreferences(verificar.getText().toString());
                        Toast.makeText(getContext(), "Pin actualizado", Toast.LENGTH_SHORT).show();
                        cambiar.setEnabled(false);
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }else{
                        Toast.makeText(getContext(), "Pin no correcto", Toast.LENGTH_SHORT).show();
                        verificar.setText(null);
                    }
                }
            }
        });

        return v;
    }

    private void sharedPreferences(String pin){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("PINPassword");
        editor.putString("PINPassword", pin);
        editor.apply();
    }

    public String testSharedPreferences(String pin){
        if(!pin.equals("")){

            return "Contraseña Registrada";
        }

        return "Contraseña No Registrada";
    }

}
