package com.orbita.innovacion.proyinte;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * Clase de inicilizacion y obtencion de datos (POJO) para el manejo de los alimentos
 * que se podran visualizar en el Fragmento Comedor
 *
 * Fecha de creación: 15/02/2018.
 * Versión: 18.2.15
 * Modificaciones:
 * Ninguna.
 */

public class Item_Comedor {
    private String nombre;
    private String descripcion;
    private String img;

    public Item_Comedor(String nombre, String descripcion, String img) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.img = img;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Bitmap getImg() {
        return base64ToBitmap(img);
    }

    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

}
