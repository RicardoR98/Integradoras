package com.orbita.innovacion.proyinte.test;

import com.orbita.innovacion.proyinte.Internet;
import com.orbita.innovacion.proyinte.pin_cambio;

import org.junit.Test;

import static org.mockito.Mockito.mock;

public class Pruebas {

    @Test
    public void test1() throws Exception {
        //assertEquals(4, 2 + 2);

        Internet internet = mock(Internet.class);

        boolean test = internet.testisOnline();

        if(test){
            System.out.println("Conectado a internet");
            PersistenciaDeDatos.CONEXION = test;
        }else {
            System.out.println("Conexion cerrada");
        }

    }

    @Test
    public void test2() throws Exception {
        if(PersistenciaDeDatos.CONEXION){
            System.out.println("Sesión iniciada");
        }else {
            System.out.println("No se pudo iniciar sesión");
        }
    }

    @Test
    public void test3() throws Exception{
        if(PersistenciaDeDatos.CONEXION){
            String pin = "0000";
            pin_cambio pin_cambio = new pin_cambio();
            String si_ = pin_cambio.testSharedPreferences(pin);

            if(si_.equals("Contraseña Registrada")){
                PersistenciaDeDatos.PIN = pin;
            }
        }else{
            System.out.println("No se pudo actualizar pin");
        }
    }

    @Test
    public void test4(){
        if(PersistenciaDeDatos.PIN != null){
            System.out.println("Pin: " + PersistenciaDeDatos.PIN);
        }
    }

}