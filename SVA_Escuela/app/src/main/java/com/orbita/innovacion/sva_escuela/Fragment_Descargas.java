package com.orbita.innovacion.sva_escuela;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static android.support.v4.content.PermissionChecker.checkSelfPermission;

public class Fragment_Descargas extends Fragment {

    private LottieAnimationView success, error;

    private EditText grupoHorario, descripcion, url;
    private Button seleccionar, publicar;
    private ImageView imagen = null;
    private Bitmap bitmap;

    private String IDCorreo, tema, nomb, grupo;

    private final int COD_SELECCIONA = 10;

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_descargas, container, false);

        grupoHorario = (EditText) v.findViewById(R.id.grupoHorarioEditText);
        descripcion = (EditText) v.findViewById(R.id.descripcionEditText);
        url = (EditText) v.findViewById(R.id.urlEditText);
        publicar = (Button) v.findViewById(R.id.btnPublicar);
        seleccionar = (Button) v.findViewById(R.id.btnSeleccionar);
        imagen = (ImageView) v.findViewById(R.id.imgCarga);
        success = (LottieAnimationView) v.findViewById(R.id.success);
        error = (LottieAnimationView) v.findViewById(R.id.error);

        IDCorreo = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("IDCorreo","null");

        DocumentReference miDocRef = FirebaseFirestore.getInstance().document("Personal/" + IDCorreo);


        miDocRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){

                    nomb = documentSnapshot.getString("nombre");
                    tema = documentSnapshot.getString("tema");
                    grupo = documentSnapshot.getString("grupo");

                    llenar();

                }
            }
        });

        seleccionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    cargaImagen();
                    success.setVisibility(View.GONE);
                    imagen.setImageBitmap(null);
                } else {
                    int permiso = checkSelfPermission(getContext(), READ_EXTERNAL_STORAGE);
                    if (permiso != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, 001);
                    } else
                        cargaImagen();
                }
            }
        });

        publicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(grupoHorario.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo nombre vacío", Toast.LENGTH_SHORT).show();
                    return;
                }else if(descripcion.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo descripcion vacío", Toast.LENGTH_SHORT).show();
                    return;
                }else if(bitmap == null){
                    Toast.makeText(getContext(), "Imagen no seleccionada", Toast.LENGTH_SHORT).show();
                    return;
                }else if(url.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo url vacío", Toast.LENGTH_SHORT).show();
                    return;
                }
                else{
                    subir();
                    publicar.setEnabled(false);
                    success.setVisibility(View.GONE);
                    imagen.setImageBitmap(null);
                }
            }
        });

        publicar.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Fragment nuevoFragmento = new Fragment_DescargasVer();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fram, nuevoFragmento);
                transaction.addToBackStack(null);
                transaction.commit();

                return false;
            }
        });

        return v;
    }

    private void subir(){
        String grupo = grupoHorario.getText().toString();
        String desc = descripcion.getText().toString();
        String URL = url.getText().toString();
        String img = bitmapToBase64(bitmap);

        Map<String, Object> data = new HashMap<>();
        data.put("contenido", desc);
        data.put("grupo", grupo);
        data.put("img", img);
        data.put("url_img", URL);

        db.collection("Horarios").add(data).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                success.setVisibility(View.VISIBLE);
                success.playAnimation();
                grupoHorario.setText(null);
                descripcion.setText(null);
                url.setText(null);
                bitmap = null;
                publicar.setEnabled(true);
                NotificarPhp NP = new NotificarPhp(
                                getContext(),
                                tema,
                                nomb,
                                " agrego el horario de su grupo",
                                "Entre a su aplicación y verifique el horario asignado");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                error.setVisibility(View.VISIBLE);
                error.playAnimation();
            }
        });
    }

    private void cargaImagen() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent, "Seleccione la Aplicación"), COD_SELECCIONA);
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void llenar() {
        grupoHorario.setText(grupo);
        grupoHorario.setEnabled(false);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("Nombre");
        editor.putString("Nombre", nomb.toString());
        editor.apply();
    }

    private Bitmap redimensionarImagen(Bitmap bitmap, float anchoNuevo, float altoNuevo) {

        int ancho=bitmap.getWidth();
        int alto=bitmap.getHeight();

        if(ancho>anchoNuevo || alto>altoNuevo){
            float escalaAncho=anchoNuevo/ancho;
            float escalaAlto= altoNuevo/alto;

            Matrix matrix=new Matrix();
            matrix.postScale(escalaAncho,escalaAlto);

            return Bitmap.createBitmap(bitmap,0,0,ancho,alto,matrix,false);

        }else{
            return bitmap;
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case COD_SELECCIONA:
                    Uri miPath = data.getData();

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),miPath);
                        bitmap = redimensionarImagen(bitmap,900,700);
                        imagen.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case COD_SELECCIONA:
                final int numOfRequest = grantResults.length;
                final boolean isGranted = numOfRequest == 001
                        && PackageManager.PERMISSION_GRANTED == grantResults[numOfRequest - 1];
                if (isGranted) {
                    cargaImagen();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
