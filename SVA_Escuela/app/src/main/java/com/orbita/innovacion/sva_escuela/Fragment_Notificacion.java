package com.orbita.innovacion.sva_escuela;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class Fragment_Notificacion extends Fragment {

    String cuer = "", tit = "";

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    private ListView lista;
    private ArrayAdapter<ListItem_Notificacion> adapter = null;
    private ArrayList<ListItem_Notificacion> alumnos = new ArrayList<>();

    private ProgressBar progressBar;

    private EditText titulo, cuerpo;
    private Button notificar;
    private String name, tema;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notificacion, container, false);

        lista = (ListView) v.findViewById(R.id.List);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        tema = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("TEMA","null");


        db.collection("Escuela")
                .whereEqualTo("tema",tema)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                alumnos.add(new ListItem_Notificacion(
                                        document.getString("nombre"),
                                        document.getString("Apellido_Paterno"),
                                        document.getString("Apellido_Materno"),
                                        document.getString("grado"),
                                        document.getString("grupo"),
                                        document.getString("Token_Notification")
                                ));
                                llenar();
                            }
                        }
                    }
                });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object fhater = (ListItem_Notificacion) parent.getItemAtPosition(position);

                String token = ((ListItem_Notificacion)fhater).getToken();

                display(token);
            }
        });

        return v;
    }

    private void llenar() {
        adapter = new ArrayAdapter<ListItem_Notificacion>(getContext(), android.R.layout.simple_list_item_1, alumnos);
        lista.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
        lista.setVisibility(View.VISIBLE);
    }

    private void display(final String token) {
        final Dialog d = new Dialog(getContext());
        d.setContentView(R.layout.notificacion_layout);

        titulo = (EditText) d.findViewById(R.id.tituloEditText);
        cuerpo = (EditText) d.findViewById(R.id.cuerpoEditText);
        notificar = (Button) d.findViewById(R.id.btnNotificar);

        name = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("Name","null");

        notificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(titulo.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo titulo vacío", Toast.LENGTH_SHORT).show();
                    return;
                }else if(cuerpo.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo cuerpo vacío", Toast.LENGTH_SHORT).show();
                    return;
                } else{
                    tit = titulo.getText().toString();
                    cuer = cuerpo.getText().toString();
                    d.cancel();
                    subir(tit, cuer, token, name);
                }
            }
        });

        d.show();
    }

    private void subir(String tit, String cuer, String token, String name){
        NotificarPhpToken NPT = new NotificarPhpToken(
                getContext(),
                name,
                token,
                tit,
                cuer
        );
    }

}
