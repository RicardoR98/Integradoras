package com.orbita.innovacion.sva_escuela;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentEventosVer extends Fragment {

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    private String nombre;

    private ProgressBar progressBar;
    private RecyclerView.Adapter adapter;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    private ArrayList<Item_Evento> items = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_eventos_ver, container, false);

        nombre = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Nombre","null");

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        items = new ArrayList<Item_Evento>();

        // Obtener el RecyclerView
        recycler = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        recycler.setHasFixedSize(false);

        // Llamada a la coleccion de Eventos para traer todos los datos
        db.collection("Eventos")
                .whereEqualTo("nombre", nombre)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (DocumentSnapshot document : task.getResult()) {
                                items.add(new Item_Evento(document.getId()
                                        ,document.getString("nombre"),
                                        document.getString("mensaje"),
                                        document.getString("img_perfil")));
                            }

                            // Usar un administrador para LinearLayout
                            lManager = new LinearLayoutManager(getContext());
                            recycler.setLayoutManager(lManager);

                            // Crear un nuevo adaptador
                            adapter = new AdapterListItem(items);
                            recycler.setAdapter(adapter);

                            progressBar.setVisibility(View.GONE);
                            recycler.setVisibility(View.VISIBLE);
                        }
                    }
                });

        return v;
    }

    //Clase anidada para adaptar datos al CardView y RecyclerView
    public class AdapterListItem extends RecyclerView.Adapter<AdapterListItem.EventoViewHolder> {
        //Creacion de lista de items
        private List<Item_Evento> items;

        public class EventoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            // Campos respectivos de un item
            private CircleImageView imagen;
            private TextView nombre;
            private TextView mensaje;
            private Button eliminar;
            private TextView id;
            private Context context;

            public EventoViewHolder(View v) {
                //Inicilizando WidGets del CardView (item_evento)
                super(v);
                context = v.getContext();
                id = (TextView) v.findViewById(R.id.txt_id_evento);
                imagen = (CircleImageView) v.findViewById(R.id.CircleImgCard);
                nombre = (TextView) v.findViewById(R.id.txtNombreCard);
                mensaje = (TextView) v.findViewById(R.id.txtMensajeCard);
                eliminar = (Button) v.findViewById(R.id.btmEliminar);
            }

            public void accesoAbotones(){
                eliminar.setOnClickListener(this);
            }

            // Boton de descarga de imagen de horario para su mejor acceso
            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.btmEliminar){
                    ventanaDialogo(id.getText().toString());
                }
            }

            public void ventanaDialogo(final String ID) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("Eliminacion");
                builder.setMessage("¿Seguro/a que desea eliminar este evento?");

                builder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Toast.makeText(context, "Acción Desechada", Toast.LENGTH_SHORT).show();

                            }
                        });

                builder.setPositiveButton("Si",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                db.collection("Eventos").document(ID)
                                        .delete()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(context, "Eliminado con exito", Toast.LENGTH_SHORT).show();

                                                Fragment nuevoFragmento = new FragmentEventosVer();
                                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                                transaction.replace(R.id.fram, nuevoFragmento);
                                                transaction.addToBackStack(null);
                                                transaction.commit();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(context, "Error al eliminar", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                            }
                        });
                builder.show();
            }

        }

        public AdapterListItem(List<Item_Evento> items) {
            //Igualando datos de la lista
            this.items = items;
        }

        @Override
        public int getItemCount() {
            //Sacando tamaño de la lista
            return items.size();
        }

        @Override
        public AdapterListItem.EventoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            //Inflando Layout de CardView (item_evento)
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_evento, viewGroup, false);
            return new AdapterListItem.EventoViewHolder(v);
        }

        @Override
        public void onBindViewHolder(AdapterListItem.EventoViewHolder viewHolder, int i) {
            //Agregando los datos al CardView (item_evento)
            viewHolder.id.setText(items.get(i).getID());
            viewHolder.imagen.setImageBitmap(items.get(i).getImg());
            viewHolder.nombre.setText(items.get(i).getNombre());
            viewHolder.mensaje.setText(items.get(i).getMensaje());
            viewHolder.accesoAbotones();
        }
    }

}
