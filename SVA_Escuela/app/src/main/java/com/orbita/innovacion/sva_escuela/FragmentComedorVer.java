package com.orbita.innovacion.sva_escuela;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class FragmentComedorVer extends Fragment {

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    private ProgressBar progressBar;
    private RecyclerView.Adapter adapter;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    private ArrayList<Item_Comedor> items = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_comedor_ver, container, false);

        items = new ArrayList<Item_Comedor>();

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        // Obtener el RecyclerView
        recycler = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        recycler.setHasFixedSize(false);

        // Llamada a la coleccion de Comedor para traer todos los datos
        db.collection("Comedor").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (DocumentSnapshot document : task.getResult()) {
                                items.add(new Item_Comedor(document.getId(),
                                        document.getString("nombre"),
                                        document.getString("descripcion"),
                                        document.getString("img")));
                            }

                            // Usar un administrador para LinearLayout
                            lManager = new LinearLayoutManager(getContext());
                            recycler.setLayoutManager(lManager);

                            // Crear un nuevo adaptador
                            adapter = new AdapterListItem(items);
                            recycler.setAdapter(adapter);

                            progressBar.setVisibility(View.GONE);
                            recycler.setVisibility(View.VISIBLE);
                        }
                    }
                });

        return v;
    }

    //Clase anidada para adaptar datos al CardView y RecyclerView
    public class AdapterListItem extends RecyclerView.Adapter<AdapterListItem.ComedorViewHolder> {
        //Creacion de lista de items
        private List<Item_Comedor> items;

        public class ComedorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            // Campos respectivos de un item
            private ImageView imagen;
            private TextView nombre;
            private TextView descripcion;
            private Button eliminar;
            private TextView id;
            private Context context;

            public ComedorViewHolder(View v) {
                //Inicilizando WidGets del CardView (item_comedor)
                super(v);
                context = v.getContext();
                id = (TextView) v.findViewById(R.id.txt_id_comedor);
                imagen = (ImageView) v.findViewById(R.id.img);
                nombre = (TextView) v.findViewById(R.id.txtNombreCard);
                descripcion = (TextView) v.findViewById(R.id.txtDescripcionCard);
                eliminar = (Button) v.findViewById(R.id.btmEliminar);

            }

            public void accesoAbotones(){
                eliminar.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.btmEliminar){
                    ventanaDialogo(id.getText().toString());
                }
            }

            public void ventanaDialogo(final String ID) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("Eliminacion");
                builder.setMessage("¿Seguro/a que desea eliminar este platillo?");

                builder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Toast.makeText(context, "Acción Desechada", Toast.LENGTH_SHORT).show();

                            }
                        });

                builder.setPositiveButton("Si",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                db.collection("Comedor").document(ID)
                                        .delete()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(context, "Eliminado con exito", Toast.LENGTH_SHORT).show();

                                                Fragment nuevoFragmento = new FragmentComedorVer();
                                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                                transaction.replace(R.id.fram, nuevoFragmento);
                                                transaction.addToBackStack(null);
                                                transaction.commit();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(context, "Error al eliminar", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                            }
                        });
                builder.show();
            }

        }

        public AdapterListItem(List<Item_Comedor> items) {
            //Igualando datos de la lista
            this.items = items;
        }

        @Override
        public int getItemCount() {
            //Sacando tamaño de la lista
            return items.size();
        }

        @Override
        public ComedorViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            //Inflando Layout de CardView (item_comedor)
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_comedor, viewGroup, false);
            return new ComedorViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ComedorViewHolder viewHolder, int i) {
            //Agregando los datos al CardView (item_comedor)
            viewHolder.id.setText(items.get(i).getID());
            viewHolder.imagen.setImageBitmap(items.get(i).getImg());
            viewHolder.nombre.setText(items.get(i).getNombre());
            viewHolder.descripcion.setText(items.get(i).getDescripcion());
            viewHolder.accesoAbotones();
        }
    }
}
