package com.orbita.innovacion.sva_escuela;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.format.Time;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NotificarPhpToken implements com.android.volley.Response.Listener<JSONObject>, com.android.volley.Response.ErrorListener{

    private int SPLASH_TIME_OUT = 3500;
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    ProgressDialog progressDialog;

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    Context con;
    String token = "", titulo = "", cuerpo = "", nombre = "";

    public NotificarPhpToken(Context context, String nom, String toke, String titul, String cuerp) {
        request = Volley.newRequestQueue(context);
        this.con = context;
        this.token = toke;
        this.titulo = titul;
        this.cuerpo = cuerp;
        this.nombre = nom;

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Enviando notificacion...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (new Internet().isOnline(con)) {
                    WebService();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(con, "Conectese a Internet", Toast.LENGTH_SHORT).show();
                    Toast.makeText(con, "Notificacion no enviada", Toast.LENGTH_SHORT).show();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void WebService() {
        String url =
                "https://demo1234demostracion.000webhostapp.com/Firebase/notificacionToken.php?" +
                        "titulo=" + nombre + ": " + titulo + "&mensage=" + cuerpo + "&id=" + token;

        url = url.replace(" ", "%20");

        jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url,
                null, this, this);

        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressDialog.dismiss();

        Time today=new Time(Time.getCurrentTimezone());
        today.setToNow();
        int dia=today.monthDay;
        int mes=today.month;
        int ano=today.year;
        mes=mes+1;

        Map<String, Object> data = new HashMap<>();
        data.put("fecha", "mes: "+ mes + " " +"dia: " + dia + " " + "año: " + ano);
        data.put("hora", new Date().toString());
        data.put("error", error.toString());
        data.put("dispositivo", Settings.Secure.getString(con.getContentResolver(), Settings.Secure.ANDROID_ID));

        db.collection("Error_PHP").add(data).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                Toast.makeText(con, "Se registro informe de error", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(con, "Error al registrar informe de error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onResponse(JSONObject response) {

        progressDialog.dismiss();
        Toast.makeText(con, "Notificacion enviada", Toast.LENGTH_SHORT).show();

    }
}
